package uk.org.llgc.annotation.store;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import java.io.*;

import java.net.URL;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import com.github.jsonldjava.utils.JsonUtils;

import org.apache.jena.rdf.model.Model;

import uk.org.llgc.annotation.store.adapters.StoreAdapter;
import uk.org.llgc.annotation.store.encoders.Encoder;
import uk.org.llgc.annotation.store.exceptions.IDConflictException;
import uk.org.llgc.annotation.store.data.ManifestProcessor;
import uk.org.llgc.annotation.store.data.Manifest;

public class ManifestUpload extends HttpServlet {
	protected static Logger _logger = LogManager.getLogger(ManifestUpload.class.getName());
	protected AnnotationUtils _annotationUtils = null;
	protected StoreAdapter _store = null;
	protected File _manifestDir = null;

	public void init(final ServletConfig pConfig) throws ServletException {
		super.init(pConfig);
		Encoder tEncoder = StoreConfig.getConfig().getEncoder();
		_annotationUtils = new AnnotationUtils(new File(super.getServletContext().getRealPath("/contexts")), tEncoder);
		_store = StoreConfig.getConfig().getStore();
		_store.init(_annotationUtils);
		_manifestDir = new File(super.getServletContext().getRealPath(pConfig.getInitParameter("manifest_dir")));
		if (!_manifestDir.exists()) {
			_logger.debug("Making " + _manifestDir.getPath());
			_manifestDir.mkdirs();
		} else {
			_logger.debug("exists " + _manifestDir.getPath());
		}
	}

	public void doPost(final HttpServletRequest pReq, final HttpServletResponse pRes) throws IOException {
		String tID = "";
		Map<String, Object> tManifest = null;

		try {
			if (pReq.getParameter("uri") != null) {
				tID = pReq.getParameter("uri");
				tManifest = (Map<String,Object>)JsonUtils.fromInputStream(new URL(tID).openStream());
			} else {
				InputStream tManifestStream = pReq.getInputStream();
				tManifest = (Map<String,Object>)JsonUtils.fromInputStream(tManifestStream);
			}

			String tShortId = _store.indexManifest(tManifest);

			PrintWriter writer = pRes.getWriter();
			String url = StoreConfig.getConfig().getBaseURI(pReq) + "/search-api/" + tShortId + "/search";
			String htmlRespone = buildResponseOk("<a href=\"" + url+ "\">link</a> to manifest search API");

			//pRes.sendRedirect(StoreConfig.getConfig().getBaseURI(pReq) + "/search-api/" + tShortId + "/search");

			// return response
			writer.println(htmlRespone);
		} catch (Exception error) {
			error.printStackTrace();

			// get response writer
			PrintWriter writer = pRes.getWriter();
			// build HTML code
			String htmlRespone = buildResponseError("Failed to upload/process manifest (already indexed ?)");
			// return response
			writer.println(htmlRespone);
		}
	}

	// if asked for without path then return collection of manifests that are loaded
	public void doGet(final HttpServletRequest pReq, final HttpServletResponse pRes) throws IOException {
		String tRequestURI = pReq.getRequestURI();
		String[] tSplitURI = tRequestURI.split("/");

		// Return collection
		List<Manifest> tManifests = _store.getManifests();

		Map<String,Object> tCollection = new HashMap<String,Object>();

		tCollection.put("@context", "http://iiif.io/api/presentation/2/context.json");
		tCollection.put("@id", StoreConfig.getConfig().getBaseURI(pReq) + "/annotation//collection/managed.json");
		tCollection.put("@type", "sc:Collection");
		tCollection.put("label","Collection of all manifests known by this annotation server");

		List<Map<String,Object>> tMembers = new ArrayList<Map<String,Object>>();
		for (Manifest tManifest : tManifests) {
			Map<String, Object> tManifestJson = new HashMap<String,Object>();
			tManifestJson.put("@id", tManifest.getURI());
			tManifestJson.put("@type", "sc:Manifest");

			tMembers.add(tManifestJson);
		}

		tCollection.put("members", tMembers);
		tCollection.put("manifests", tMembers);

		pRes.setStatus(HttpServletResponse.SC_CREATED);
		pRes.setContentType("application/ld+json; charset=UTF-8");
		pRes.setCharacterEncoding("UTF-8");
		JsonUtils.write(pRes.getWriter(), tCollection);
	}


	String buildResponseError(String errorMessage) {
		return resp_part1 + "<p>" + errorMessage + "</p>" + resp_part2;
	}

	String buildResponseOk(String responseMessage) {
		return resp_part1 + "<p>" + responseMessage + "</p>" + resp_part2;
	}

	String resp_part1 = "<!DOCTYPE html SYSTEM \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
				"\"<html xmlns=\"http://www.w3.org/1999/xhtml\">\"" +
				"<head>" +
				"<meta charset=\"utf-8\">" +
				"<meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\">" +
				"<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">" +
				"<title>Register manifest with annotation store</title>" +
				"<link rel=\"stylesheet\" href=\"../css/bootstrap-local.min.css\">" +
				"<link rel=\"stylesheet\" href=\"../css/dashboard.css\">" +
				"<link rel=\"stylesheet\" href=\"../css/local.css\">" +
				"</head>" +
				"<body>" +
				"<div role=\"navigation\" class=\"navbar navbar-inverse navbar-fixed-top\">" +
				"<div class=\"container-fluid\">" +
				"<div class=\"navbar-collapse collapse\">" +
				"<ul class=\"nav navbar-nav\">" +
				"<li>" +
				"<a href=\"index.html\">Home</a>" +
				"</li>" +
				"<li>" +
				"<a href=\"stats/index.html\">Stats</a>" +
				"</li>" +
				"<li>" +
				"<a href=\"populate.html\">Load Annotation List</a>" +
				"</li>" +
				"<li>" +
				"<a href=\"uploadManifest.html\">Index Manifest</a>" +
				"</li>" +
				"<li>" +
				"<a href=\"extractManifest.html\">Extract Manifest</a>" +
				"</li>" +
				"</ul>" +
				"</div>" +
				"</div>" +
				"</div>" +
				"<main xmlns=\"http://www.w3.org/1999/xhtml\" tabindex=\"-1\" role=\"main\" id=\"content\" class=\"bs-docs-masthead\">" +
				"<div class=\"container\">" +
				"<h1>Register manifest with annotation store</h1>" +
				"<p>This page allows you to upload a link to a manifest so that it is registered with the repository. Annotations which are associated with this canvas are then linked to this manifest and also a IIIF Search API endpoint is created.</p>" +
				"<form action=\"manifests\" method=\"post\">" +
				"<p><b>URL to Manifest:</b> <input type=\"text\" name=\"uri\" id=\"uri\" size=\"100\"/></p>" +
				"<br/>";

	String resp_part2 = "<input type=\"submit\" value=\"Submit\"/>" +
				"</form>  " +
				"</div>" +
				"</main>" +
				"<div style=\"text-align: center;\" class=\"footer_row\">" +
				"<div class=\"footer_item\"><img height=\"60\" src=\"/images/logo_enpc2.png\"/><img height=\"60\" src=\"/images/logo_inria2.png\"/></div>" +
				"</div>" +
				"</body>" +
				"</html>";

}


