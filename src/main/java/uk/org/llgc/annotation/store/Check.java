package uk.org.llgc.annotation.store;

import com.github.jsonldjava.utils.JsonUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.org.llgc.annotation.store.adapters.StoreAdapter;
import uk.org.llgc.annotation.store.encoders.Encoder;

import javax.json.*;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Map;

public class Check extends HttpServlet {
	protected static Logger _logger = LogManager.getLogger(Check.class.getName());

	protected AnnotationUtils _annotationUtils = null;
	protected StoreAdapter _store = null;

	public void init(final ServletConfig pConfig) throws ServletException {
		super.init(pConfig);
		Encoder tEncoder = StoreConfig.getConfig().getEncoder();

		_annotationUtils = new AnnotationUtils(new File(super.getServletContext().getRealPath("/contexts")), tEncoder);
		_store = StoreConfig.getConfig().getStore();
		_store.init(_annotationUtils);
	}


	public void doPost(final HttpServletRequest pReq, final HttpServletResponse pRes) throws IOException {

		if (pReq.getParameter("jobid") != null) {
			String jobId =  pReq.getParameter("jobid");

			_logger.debug("Check results for job " + jobId);

			try {
				String url = "http://pgia.paris.inria.fr:80/dex/api/check";

				String result = "";
				HttpPost post = new HttpPost(url);

				// add request parameters or form parameters
				String JSON_STRING="{\"jobId\":\"" + jobId + "\"}";
				HttpEntity stringEntity = new StringEntity(JSON_STRING, ContentType.APPLICATION_JSON);
				post.setEntity(stringEntity);

				try (CloseableHttpClient httpClient = HttpClients.createDefault();
					 CloseableHttpResponse response = httpClient.execute(post)) {
					result = EntityUtils.toString(response.getEntity());
				}

				//pRes.setStatus(HttpServletResponse.SC_CREATED);
				//pRes.setContentType("text/plain");
				//pRes.getOutputStream().println(result);

				// get response writer
				PrintWriter writer = pRes.getWriter();
				// build HTML code
				String htmlRespone = buildResponseOk(jobId, result);
				// return response
				writer.println(htmlRespone);
				return;
			} catch (Exception error) {
				error.printStackTrace();
				//pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				//pRes.setContentType("text/plain");
				//pRes.getOutputStream().println("Unable to fullfil request");

				// get response writer
				PrintWriter writer = pRes.getWriter();
				// build HTML code
				String htmlRespone = buildResponseError(error.getMessage());
				// return response
				writer.println(htmlRespone);
			}

		} else {
			//pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			//pRes.setContentType("text/plain");
			// pRes.getOutputStream().println("Invalid manifest URL parameter: " + pReq.getParameter("uri") );

			// get response writer
			PrintWriter writer = pRes.getWriter();
			// build HTML code
			String htmlRespone = buildResponseError("unable to process check");
			// return response
			writer.println(htmlRespone);
		}

	}

	// unused call (used to validate parsing of annotation list)
	public void doPost2(final HttpServletRequest pReq, final HttpServletResponse pRes) throws IOException {

		if (pReq.getParameter("uri") != null) {
			_logger.debug("Reading from " + pReq.getParameter("uri"));

			String filename = "/WEB-INF/annot.json";
			ServletContext context = getServletContext();

			// First get the file InputStream using ServletContext.getResourceAsStream()
			// method.
			String allfile = "";
			InputStream is = context.getResourceAsStream(filename);

			/**
			if (is != null) {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(isr);

				String text;

				// We read the file line by line and later will be displayed on the
				// browser page.
				while ((text = reader.readLine()) != null) {
					allfile += text;
				}
			}
			System.out.println("-----------");
			System.out.println(allfile);
			System.out.println("-----------");
			**/

			/**
			String initialString = "{\n"+
									"	\"jobId\" : \"toto\"\n" +
									"}";
			InputStream issimple = new ByteArrayInputStream(allfile.getBytes());

			File annotFile = new File("WEB-INF/annot.json");
			// InputStream targetStream = new FileInputStream(annotFile);
			InputStream targetStream = pReq.getSession().getServletContext().getResourceAsStream("WEB-INF/annot.json");

			 **/

			/**
			JsonReader rdr = Json.createReader(issimple);
			JsonObject obj = rdr.readObject();
			String jobId = (String) obj.getString("@id");
			JsonArray resources = obj.getJsonArray("resources");
			**/

			String jobId = "";

			List<Map<String, Object>> tAnnotationListJSON = _annotationUtils.readAnnotationList(is, StoreConfig.getConfig().getBaseURI(pReq) + "/annotation"); //annotaiton list
			_logger.debug("JSON in:");
			_logger.debug(JsonUtils.toPrettyString(tAnnotationListJSON));

			try {
				_store.addAnnotationList(tAnnotationListJSON);

				//pRes.setStatus(HttpServletResponse.SC_CREATED);
				//pRes.setContentType("text/plain");
				//pRes.getOutputStream().println("SUCCESS");

				// get response writer
				PrintWriter writer = pRes.getWriter();
				// build HTML code
				String htmlRespone = ""; // buildResponse(jobId, null);

				// return response
				writer.println(htmlRespone);

			} catch (Exception tException) {
				tException.printStackTrace();
				pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				pRes.setContentType("text/plain");
				pRes.getOutputStream().println("Failed to load annotation list as there was a conflict in ids " + tException.toString());
			}


/*
			URL url = new URL(pReq.getParameter("uri"));
			try (InputStream is = url.openStream();

				JsonArray results = obj.getObj"id").;
				for (JsonObject result : results.getValuesAs(JsonObject.class)) {
					System.out.print(result.getJsonObject("from").getString("name"));
					System.out.print(": ");
					System.out.println(result.getString("message", ""));
					System.out.println("-----------");
				}
				*/

/*
			try {
				String url = "http://pgia.paris.inria.fr:80/dex/api/annotate";

				String result = "";
				HttpPost post = new HttpPost(url);

				// add request parameters or form parameters
				String JSON_STRING="{\"manifestUrls\":[\"" + pReq.getParameter("uri") + "\"]}";
				HttpEntity stringEntity = new StringEntity(JSON_STRING, ContentType.APPLICATION_JSON);
				post.setEntity(stringEntity);

				// List<NameValuePair> urlParameters = new ArrayList<>();
				// urlParameters.add(new BasicNameValuePair("manifestUrl", pReq.getParameter("uri")));
				// post.setEntity(new UrlEncodedFormEntity(urlParameters));

				System.out.println("execute the post request");

				try (CloseableHttpClient httpClient = HttpClients.createDefault();
					 CloseableHttpResponse response = httpClient.execute(post)) {

					result = EntityUtils.toString(response.getEntity());
				}

				pRes.setStatus(HttpServletResponse.SC_CREATED);
				pRes.setContentType("text/plain");
				pRes.getOutputStream().println(result);

				System.out.println("result returned");

				return;
			} catch (Exception error) {
				System.out.println("exception 1");

				error.printStackTrace();
				pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				pRes.setContentType("text/plain");
				pRes.getOutputStream().println("Unable to fullfil request");
			}
		*/
		} else {
			pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			pRes.setContentType("text/plain");
			pRes.getOutputStream().println("Invalid manifest URL parameter: " + pReq.getParameter("uri") );
		}


/**
		HashMap values = new HashMap<String, String>() {{
			put("name", "John Doe");
			put ("occupation", "gardener");
		}};

		ObjectMapper objectMapper = new ObjectMapper();
		String requestBody = objectMapper
				.writeValueAsString(values);

		HttpClient client = new HttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("https://httpbin.org/post"))
				.POST(HttpRequest.BodyPublishers.ofString(requestBody))
				.build();

		HttpResponse<String> response = client.send(request,
				HttpResponse.BodyHandlers.ofString());

		System.out.println(response.body());


		List<Map<String, Object>> tAnnotationListJSON = _annotationUtils.readAnnotationList(tAnnotationList, StoreConfig.getConfig().getBaseURI(pReq) + "/annotation"); //annotaiton list
		_logger.debug("JSON in:");
		_logger.debug(JsonUtils.toPrettyString(tAnnotationListJSON));

		try {
			_store.addAnnotationList(tAnnotationListJSON);

			pRes.setStatus(HttpServletResponse.SC_CREATED);
			pRes.setContentType("text/plain");
			pRes.getOutputStream().println("SUCCESS");
		} catch (IDConflictException tException) {
			tException.printStackTrace();
			pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			pRes.setContentType("text/plain");
			pRes.getOutputStream().println("Failed to load annotation list as there was a conflict in ids " + tException.toString());
		} catch (MalformedAnnotation tExcpt) {
			tExcpt.printStackTrace();
			pRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			pRes.setContentType("text/plain");
			pRes.getOutputStream().println("Falied to load annotation as it was badly informed: " + tExcpt.toString());
		}

 */
	}



	String getExtractResultOk (String jobId, JsonObject status, String annotationStatus) {
		String str = "<p>Status of job ID " + jobId + "</p>";
		str += "<p>" + annotationStatus + "</p>";
		str += "<table>";
		str += "<thead><tr>";
		str += "<th style=\"padding:5px\" colspan=\"1\">manifest</th>";
		str += "<th style=\"padding:5px\" colspan=\"1\">status</th>";
		str += "<th style=\"padding:5px\" colspan=\"1\">date</th>";
		str += "</tr></thead>";
		str += "<tbody>";

		for (Map.Entry<String, JsonValue> entry : status.entrySet()) {

			String key = entry.getKey();
			JsonValue value = entry.getValue();

			JsonObject innerItem = (JsonObject)value;

			str += "<tr>";
			str += "<td style=\"padding:5px\" >"+key+"</td><td style=\"padding:5px\" >"+innerItem.getString("status")+"</td><td style=\"padding:5px\" >"+innerItem.getString("date")+"</td>";
			str += "</tr>";

		}

		str += "</tbody></table>";

		return str;
	}

	String getExtractResultError (String errorCode) {
		return "<p>Unable to check job (error: " + errorCode + ").</p>";
	}

	String buildResponseOk(String jobId, String jobResult) {

		InputStream	stream =  new ByteArrayInputStream(jobResult.getBytes());
		JsonReader rdr = Json.createReader(stream);
		JsonObject obj = rdr.readObject();

		JsonObject status = obj.getJsonObject("jobStatus");
		JsonObject annotations = obj.getJsonObject("annotations");

		String annotationStatus = "Annotation job is still running (pending), has failed (error) or is over (done)";

		for (Map.Entry<String, JsonValue> annotation : annotations.entrySet()) {
			String key = annotation.getKey();
			JsonValue value = annotation.getValue();
			JsonObject innerItem = (JsonObject)value;

			final StringWriter writer = new StringWriter();
			final JsonWriter jwriter = Json.createWriter(writer);
			jwriter.writeObject(innerItem);
			InputStream tAnnotationList =  new ByteArrayInputStream (writer.toString().getBytes());

			try {
				// String baseUrl = StoreConfig.getConfig().getBaseURI(pReq) + "/annotation";
				String baseUrl = "https://enherit.paris.inria.fr/docextractor/" + key + "/annotation";

				List<Map<String, Object>> tAnnotationListJSON = _annotationUtils.readAnnotationList(tAnnotationList, baseUrl); //annotaiton list
				_logger.debug("loaded annotation list:");

				_store.addAnnotationList(tAnnotationListJSON);
				_logger.debug("Imported annotation list:");

				indexManifest(key);
				_logger.debug("Indexed manifest:");

				annotationStatus = "Automatic annotations uploaded (" + tAnnotationListJSON.size() + ")";

			} catch (Exception tException) {
				annotationStatus = "Failed to load automatic annotations (" + tException.getMessage() + ")";
				tException.printStackTrace();
			}

		}
		return resp_part1 + resp_part2 + getExtractResultOk(jobId, status, annotationStatus)  + resp_part3;
	}

	private void indexManifest(String manifestUrl) throws IOException {
		ServletContext context = getServletContext();

		try {
			// use direct localhost as https/ssl cannot be validated without creating keystore
			// TODO : use constants or build using servelt configuration
			String url = "http://127.0.0.1:8080/manifests";

			String result = "";
			HttpPost post = new HttpPost(url);

			// add request parameters or form parameters
			String TEXT_STRING="uri=" + manifestUrl ;
			HttpEntity stringEntity = new StringEntity(TEXT_STRING, ContentType.APPLICATION_FORM_URLENCODED);
			post.setEntity(stringEntity);

			try (CloseableHttpClient httpClient = HttpClients.createDefault();
				 CloseableHttpResponse response = httpClient.execute(post)) {
				result = EntityUtils.toString(response.getEntity());
			}

			return;
		} catch (Exception error) {
			error.printStackTrace();
		}
	}


	String buildResponseError(String errorCpde) {
		return resp_part1 + resp_part2 + getExtractResultError(errorCpde) + resp_part3;
	}


	String resp_part1 = "<!DOCTYPE html SYSTEM \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
			"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
			"    <head>\n" +
			"\t \t<meta charset=\"utf-8\">\n" +
			"\t\t<meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\">\n" +
			"\t\t<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" +
			"\t\t<title>Extract annotations from manifest with doc-extractor</title>\n" +
			"\t\t<link rel=\"stylesheet\" href=\"../css/bootstrap-local.min.css\">\n" +
			"\t\t<link rel=\"stylesheet\" href=\"../css/dashboard.css\">\n" +
			"\t\t<link rel=\"stylesheet\" href=\"../css/local.css\">\n" +
			"\t</head>\n" +
			"\t<body>\n" +
			"\t<div role=\"navigation\" class=\"navbar navbar-inverse navbar-fixed-top\">\n" +
			"\t\t<div class=\"container-fluid\">\n" +
			"\t\t\t<div class=\"navbar-collapse collapse\">\n" +
			"\t\t\t\t<ul class=\"nav navbar-nav\">\n" +
			"\t\t\t\t\t<li>\n" +
			"\t\t\t\t\t\t<a href=\"/index.html\">Home</a>\n" +
			"\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t<li>\n" +
			"\t\t\t\t\t\t<a href=\"/stats/index.html\">Stats</a>\n" +
			"\t\t\t\t\t</li>\n" +
            /*
			"\t\t\t\t\t<li>\n" +
			"\t\t\t\t\t\t<a href=\"/populate.html\">Load Annotation List</a>\n" +
			"\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t<li>\n" +
			"\t\t\t\t\t\t<a href=\"/uploadManifest.html\">Index Manifest</a>\n" +
			"\t\t\t\t\t</li>\n" +
            */
			"\t\t\t\t\t<li>\n" +
			"\t\t\t\t\t\t<a href=\"/extractManifest.html\">Extract Manifest</a>\n" +
			"\t\t\t\t\t</li>\n" +
			"\t\t\t\t</ul>\n" +
			"\t\t\t</div>\n" +
			"\t\t</div>\n" +
			"\t</div>\n" +
			"\t<main xmlns=\"http://www.w3.org/1999/xhtml\" tabindex=\"-1\" role=\"main\" id=\"content\" class=\"bs-docs-masthead\">\n" +
			"\n" +
			"\t\t<div class=\"container\">\n" +
			"        <h1>Extract manifest</h1>\n" +
			"        <p>This page allows you to upload a manifest link to annotate all elements in the corresponding document. The first form will launch the extraction on an external server with a dedicated job ID. Please keep this job ID somewhere, you will need it to retrieve the annotations in the web application through the second form once the job is done.</p>\n" +
            "       <p>Fill in a link to a manifest. You can use a search engine such as <a href='https://iiif.biblissima.fr/collections/'>https://iiif.biblissima.fr/collections/</a> to get manifest links.</p>\n" +
			"        \n" +
			"        <form action=\"/annotation/extract\" method=\"post\" style=\"margin-bottom:1em\">\n" +
			"            <p><b>URL to Manifest:</b> <input type=\"text\" name=\"uri\" id=\"uri\" size=\"100\"/></p>\n" +
			"            <br/>\n" +
			"            <input type=\"submit\" value=\"Submit\"/>\n" +
			"        </form>    \n" +
			"        <p></p>\n";

	String resp_part2 =
			"        <p>Check the job status and upload annotations if done:</p>\n" +
					"        <form action=\"/annotation/check\" method=\"post\">\n" +
					"            <p><b>Job ID:</b> <input type=\"text\" name=\"jobid\" id=\"jobid\" size=\"100\"/></p>\n" +
					"            <br/>\n" +
					"            <input type=\"submit\" value=\"Submit\"/>\n" +
					"        </form>    \n" +
					"        <p></p>\n";

	String resp_part3 =
			"\t  </div>\n" +
					"\t</main>\n" +
					"<div style=\"text-align: center;\" class=\"footer_row\"><div class=\"footer_item\"><img height=\"60\" src=\"/images/logo_enpc2.png\"/><img height=\"60\" src=\"/images/logo_inria2.png\"/></div></div>" +
					"    </body>\n" +
					"</html>";
}
